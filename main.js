// In the main process.
const { app, BrowserWindow } = require('electron')
var Mousetrap = require('mousetrap')

let win;
// Or use `remote` from the renderer process.
// const { BrowserWindow } = require('electron').remote

function createWindow () {
    win = new BrowserWindow({ width: 800, height: 600, fullscreen: true });

    win.on('closed', () => {
        win = null
    });

    win.once('ready-to-show', () => {
        win.show();
        win.setFullScreen(true);
    });

    var now = new Date();
    var future = new Date('May 29, 2019 11:13:00');

    //console.log(now < future);

    if (now < future) {
        win.loadFile('index.html');
    } else {
        win.loadFile('error.html');
    }
}

app.on('ready', createWindow)

// Quit when all windows are closed.
app.on('window-all-closed', () => {
    // On macOS it is common for applications and their menu bar
    // to stay active until the user quits explicitly with Cmd + Q
    if (process.platform !== 'darwin') {
        app.quit()
    }
})

app.on('activate', () => {
    // On macOS it's common to re-create a window in the app when the
    // dock icon is clicked and there are no other windows open.
    if (win === null) {
        createWindow()
    }
})